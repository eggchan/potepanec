require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:product) { create(:product) }
    let!(:product_2) { create(:product) }
    let!(:product_3) { create(:product) }
    let!(:taxon) { create(:taxon) }

    context "related_products.size is less than 4" do
      before do
        taxon.products << [product, product_2, product_3]
        get :show, params: { id: product.id }
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "renders app/views/potepan/products/show.html.erb" do
        expect(response).to render_template :show
      end

      it "contains appropriate product" do
        expect(assigns(:product)).to eq(product)
      end

      it "contains appropriate related_products" do
        expect(assigns(:related_products)).to match_array([product_2, product_3])
      end
    end

    context "related_products.size is more than 4" do
      let!(:product_4) { create(:product) }
      let!(:product_5) { create(:product) }
      let!(:product_6) { create(:product) }

      before do
        taxon.products << [product, product_2, product_3, product_4, product_5, product_6]
        get :show, params: { id: product.id }
      end

      it "contains appropriate related_products size" do
        expect(assigns(:related_products).size).to eq(4)
      end
    end
  end
end
