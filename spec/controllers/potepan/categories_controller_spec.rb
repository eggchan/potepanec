require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "GET #show" do
    before do
      get :show, params: { id: taxon.id }
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "renders potepan/categories/index.html.erb" do
      expect(response).to render_template :show
    end

    it "has all taxonomies object" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it "has appropriate taxon" do
      expect(assigns(:taxon)).to match(taxon)
    end

    it "has appropriate products" do
      expect(assigns(:products)).to match_array(product)
    end
  end
end
