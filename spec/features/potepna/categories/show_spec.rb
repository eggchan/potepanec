require 'rails_helper'

RSpec.feature "CategoryShow", type: :feature do
  let(:taxonomy1) { create(:taxonomy, name: 's') }
  let(:taxonomy2) { create(:taxonomy, name: 't') }
  let(:root1) { taxonomy1.root }
  let(:root2) { taxonomy2.root }
  let(:taxon1) { create(:taxon, name: 'root1_child', taxonomy: taxonomy1, parent: root1) }
  let(:taxon2) { create(:taxon, name: 'root2_child', taxonomy: taxonomy2, parent: root2) }
  let(:product1) { create(:product) }
  # priceの値は適当
  let(:product2) { create(:product, price: 19.00) }
  let(:product3) { create(:product, price: 20.00) }

  before do
    taxon1.products << [product1, product2]
    taxon2.products << [product3]
  end

  scenario "商品一覧からカテゴリーを一つ選択するとそれに属する商品達が表示され,それをクリックするとその商品詳細に飛ぶ。" do
    visit potepan_category_path(root1.id)

    expect(page).to have_content taxonomy1.name
    expect(page).to have_content taxonomy2.name

    click_link taxon1.name
    expect(page).to have_current_path potepan_category_path(taxon1.id)
    expect(page).to have_content product1.name
    expect(page).to have_content product1.display_price
    expect(page).to have_content product2.name
    expect(page).to have_content product2.display_price
    expect(page).not_to have_content product3.name
    expect(page).not_to have_content product3.display_price

    click_link taxon2.name
    click_link product3.name
    expect(page).to have_current_path potepan_product_path(product3.id)
    expect(page).to have_content product3.name
    expect(page).to have_content product3.display_price
  end
end
