require 'rails_helper'

RSpec.feature "ProductShows", type: :feature do
  let(:product) { create(:product) }
  # priceの値は適当
  let(:product_2) { create(:product, price: 19.00) }
  let(:product_3) { create(:product, price: 20.00) }
  let(:product_4) { create(:product, price: 22.00) }
  let(:taxon) { create(:taxon) }

  before do
    taxon.products << [product, product_2, product_3]
    visit potepan_product_path(product.id)
  end

  scenario "products/show page has appropriate view" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description

    within ".productsContent" do
      expect(page).to have_content product_2.name
      expect(page).to have_content product_2.display_price
      expect(page).to have_content product_3.name
      expect(page).to have_content product_3.display_price
      expect(page).not_to have_content product.name
      expect(page).not_to have_content product.display_price
      expect(page).not_to have_content product_4.name
      expect(page).not_to have_content product_4.display_price
    end

    click_link product_2.name
    expect(page).to have_current_path potepan_product_path(product_2.id)
  end

  scenario "light_section have root link" do
    within ".breadcrumb" do
      click_link "Home"
    end
    expect(page).to have_current_path potepan_root_path
  end

  scenario "header has appropriate root link" do
    within ".nav" do
      click_link "Home"
    end
    expect(page).to have_current_path potepan_root_path
  end

  scenario "has appropriate root link" do
    click_link "一覧ページへ戻る"
    expect(page).to have_current_path potepan_root_path
  end
end
