require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "#relatexd_prodcuts" do
    let!(:product_1) { create(:product) }
    let!(:product_2) { create(:product) }
    let!(:parent_taxon) { create(:taxon, name: 'Parent') }
    let!(:child_taxon) { create(:taxon, name: 'Child 1') }

    before do
      product_1.taxons << [parent_taxon, child_taxon]
      product_2.taxons << [parent_taxon, child_taxon]
    end

    it "calling product.in_taxon should not return duplicate records and self record" do
      expect(product_1.related_products).to match_array([product_2])
    end
  end
end
