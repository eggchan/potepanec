class Potepan::ProductsController < ApplicationController
  MAX_DISPLAYABLE_PRODUCT_SIZE = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:default_price, :images]).sample(MAX_DISPLAYABLE_PRODUCT_SIZE)
  end
end
